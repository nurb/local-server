<?php


namespace App\Controller;

use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\BookingObject\BookingObjectHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Tests controller.
 *
 * @Route("test")
 */
class TestController extends Controller
{
    /**
     * @Route("/", name="test-filter")
     */
    public function testFilterAction(ApiContext $apiContext)
    {

        try {
            $respoonse = $apiContext->getObjects([
                'name' => 'Karven',
                'type' => 'pension',
                'min_rate' => 300,
                'max_rate' => 6000
            ]);
        } catch (ApiException $e) {
            $respoonse = $e->getMessage();
        }

       // dump($this->getUser()->getUsername());

        return $this->render('error.html.twig', [

        ]);
    }

    /**
 * @Route("/object/{id}", requirements={"id": "\d+"}, name="test-object")
 * @Method({"GET", "POST"})
 * @param int $id
 * @param ApiContext $apiContext
 * @return Response
 */
    public function oneObjectAction(int $id, ApiContext $apiContext)
    {
        $error = null;
        $object = null;
        try {
            $data = $apiContext->showObjectReservation($id);
            dump($data);
        } catch (ApiException $e) {
            $error = 'Error: ' . $e->getMessage();
        }

        return $this->render('error.html.twig', [

        ]);
    }

    /**
     * @Route("/book", name="test-booking")
     * @Method({"GET", "POST"})
     * @param ApiContext $apiContext
     * @return Response
     */
    public function bookingObjectAction(ApiContext $apiContext)
    {
        $error = null;
        $object = null;
        try {
            $data = $apiContext->bookObject([
                'check-in' => '2018-07-05 00:00:00',
                'check-out' => '2018-07-12 00:00:00',
                'tenant' => $this->getUser()->getUsername(),
                'object' => 1

            ]);
            dump($data);
        } catch (ApiException $e) {
            $error = 'Error: ' . $e->getMessage();
        }

        return $this->render('error.html.twig', [
            'error' => $error
        ]);
    }
}