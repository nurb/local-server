<?php


namespace App\Controller;

use App\Entity\Client;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ULoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/client/social-reg-first", name="social-reg-first")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $client = new Client();
        $client->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $client, [
                'action' => $this
                    ->get('router')
                    ->generate('social-reg-second')
            ]
        );

        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('clients/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/client/social-reg-second", name="social-reg-second")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param ClientHandler $clientHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        ClientHandler $clientHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $client = new Client();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $client
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($client->getPassport(), $client->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $client->setPassword("social------------------------" . time());
                    $client->setSocialId($userData['network'], $userData['uid']);

                    $data = $client->__toArray();
                    dump($data);

                    $apiContext->createClient($data);

                    $client = $clientHandler->createNewClient($data);

                    $manager->persist($client);
                    $manager->flush();

                    return $this->redirectToRoute("homepage");
                }
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render('clients/ul_2_state.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/client/social-login", name="social-login")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     * @internal param Request $request
     * @internal param ObjectManager $manager
     */
    public function loginBySocialAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $centralData = json_decode($s, true);
        $client = $clientHandler->uLoginAuth($centralData, $clientRepository);
        if ($client) {
            $clientHandler->makeClientSession($client);
            return $this->redirectToRoute('profile');
        }
        try {
            $centralData = $apiContext->getClientBySocialId($centralData['network'], $centralData['uid']);
            $client = $clientRepository->findOneByEmail($centralData['email']);
            if ($client) {
                $client = $clientHandler->updateClient($centralData, $client);
            } else {
                $client = $clientHandler->createNewClient(
                    $centralData,
                    false
                );
            }
            $manager->persist($client);
            $manager->flush();
            $clientHandler->makeClientSession($client);

        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                'Данная социальная учетная запись не привязана'
            );
            return $this->redirectToRoute('profile');
        }

        return $this->redirectToRoute('profile');
    }
}