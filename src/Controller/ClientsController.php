<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Clients controller.
 *
 * @Route("client")
 */
class ClientsController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param ClientHandler $clientHandler
     * @return Response
     */
    public function registerAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        ClientHandler $clientHandler
    )
    {
        $error = null;
        $client = new Client();
        $form = $this->createForm("App\Form\RegisterType", $client);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($client->getPassport(), $client->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $client->getEmail(),
                        'passport' => $client->getPassport(),
                        'password' => $client->getPassword(),
                        'roles' => $client->getRoles(),
                    ];

                    $apiContext->createClient($data);

                    $client = $clientHandler->createNewClient($data);
                    $manager->persist($client);
                    $manager->flush();
                    $this->addFlash(
                        'notice',
                        'Позравляем, ваша учетная успешно создана, пожалуйста авторизируйтесь'
                    );

                    return $this->redirectToRoute("login");
                }
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render('clients/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @param ClientHandler $clientHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function loginAction(
        ClientRepository $clientRepository,
        Request $request,
        ClientHandler $clientHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;
        $form = $this->createForm("App\Form\LoginType");
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $client = $clientRepository->getByCredentials(
                $data->getEmail(),
                $data->getPassword()
            );

            if ($client) {
                $clientHandler->makeClientSession($client);
                return $this->redirectToRoute('homepage');
            }
            try {
                if ($apiContext->checkClientCredentials(
                    $data->getEmail(),
                    $data->getPassword()
                )) {
                    $centralData = $apiContext->getClientByEmail($data->getEmail());
                    $client = $clientHandler->createNewClient(
                        $centralData,
                        false
                    );
                    $manager->persist($client);
                    $manager->flush();
                    $clientHandler->makeClientSession($client);
                    return $this->redirectToRoute('homepage');
                } else {
                    $this->addFlash(
                        'error',
                        'Введенные email или пароль не найдены'
                    );
                }
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render(
            '/clients/login.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/profile", name="profile")
     * @return Response
     */
    public function profileAction()
    {
        $client = $this->getUser();
        return $this->render('clients/profile.html.twig', [
            'client' => $client
        ]);

    }


    /**
     * @Route("/bind_social", name="bind-social")
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @return Response
     */
    public function bindSocialAccountAction(ClientHandler $clientHandler, ObjectManager $manager, ApiContext $apiContext)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userRawData = json_decode($s, true);
        $client = $this->getUser();
        try {
            $data = $clientHandler->bindSocialAccount($userRawData, $client);
            $this->addFlash(
                'notice',
                'Вы успешно приявязали социальную сеть к своему аккаунту'
            );
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        try {
            $apiContext->updateClient($data);
            $manager->persist($client);
            $manager->flush();
        } catch (ApiException $a) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }


        return $this->redirectToRoute('profile');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public
    function logout()
    {

    }


}



