<?php

namespace App\Controller;


use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\BookingObject\BookingObjectHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, ApiContext $apiContext, BookingObjectHandler $bookingObjectHandler)
    {
        $objects = null;
        $form = $this->createForm(
            'App\Form\FilterType'
        );
        try {
            $objects = $bookingObjectHandler->showObjects($apiContext, [
                'name' => '',
                'type' => '',
                'min_rate' => 0,
                'max_rate' => 999999,
            ]);
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $objects = $bookingObjectHandler->showObjects($apiContext, $data);
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render('homepage.html.twig', [
            'form' => $form->createView(),
            'objects' => $objects
        ]);
    }

}
