<?php

namespace App\Controller;

use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\BookingObject\BookingObjectHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Objects controller.
 *
 * @Route("booking-object")
 */
class BookingObjectsController extends Controller
{
    const OBJECT_DATA = 'obhect_spec';

    /**
     * @Route("/add", name="add-new-object")
     * @param Request $request
     * @return Response
     */
    public function addObjectAction(Request $request)
    {
        if (!in_array('ROLE_LANDLORD', $this->getUser()->getRoles())) {
            $this->addFlash(
                'error', 'Вы не арендадатель'
            );

            return $this->redirectToRoute('homepage');
        }

        $bookingObject = new BookingObject();

        $form = $this->createForm(
            'App\Form\ObjectType', $bookingObject
        );
        $form->handleRequest($request);

        $this->redirectToRoute('spec-new-object');

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('session')->set(self::OBJECT_DATA, $bookingObject);
            return $this->redirectToRoute('spec-new-object');
        }

        return $this->render('objects/add_object.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/spec", name="spec-new-object")
     * @param ApiContext $apiContext
     * @param Request $request
     * @return Response
     * @internal param Request $request
     */
    public function specObjectAction(ApiContext $apiContext, Request $request)
    {
        /** @var $bookingObject BookingObject */
        $bookingObject = $this->get('session')->get(self::OBJECT_DATA);

        $object = null;
        switch ($bookingObject->getType()) {
            case 'cottage':
                $object = new Cottage();
                $form = $this->createForm(
                    'App\Form\CottageType', $object
                );
                break;
            case 'pension':
                $object = new Pension();
                $form = $this->createForm(
                    'App\Form\PensionType', $object
                );
                break;
            default:
                $form = null;
                break;
        }
        $form->handleRequest($request);

        $object->setName($bookingObject->getName());
        $object->setAddress($bookingObject->getAddress());
        $object->setContact($bookingObject->getContact());
        $object->setNumberOfRooms($bookingObject->getNumberOfRooms());
        $object->setType($bookingObject->getType());
        $object->setRates($bookingObject->getRates());

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $apiContext->createBookingObject($object->toArray($this->getUser()), $bookingObject->getType());
                return $this->redirectToRoute('homepage');
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
        }

        return $this->render('objects/add_object_second.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/book/{object}", requirements={"id": "\d+"},name="booking")
     * @Method({"GET"})
     * @param int $object
     * @param ApiContext $apiContext
     * @param Request $request
     * @return Response
     */
    public function bookObjectAction(int $object, ApiContext $apiContext, Request $request)
    {
        if (!in_array('ROLE_TENANT', $this->getUser()->getRoles())) {
            $this->addFlash(
                'error', 'Вы арендадатель'
            );

            return $this->redirect($request->headers->get('referer'));
        }

        if ($apiContext->isActualBookingExists($this->getUser()->getEmail())) {
            $this->addFlash(
                'error', 'Вы уже бронировали'
            );

        }
        $check_in = $request->query->get('check_in');
        $check_out = $request->query->get('check_out');
        try {
            $apiContext->bookObject([
                'check-in' => $check_in,
                'check-out' => $check_out,
                'tenant' => $this->getUser()->getUsername(),
                'object' => $object
            ]);
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/prop/{id}", requirements={"id": "\d+"}, name="showOneObject")
     * @Method({"GET", "POST"})
     * @param int $id
     * @param ApiContext $apiContext
     * @return Response
     */
    public function getObjectAction(int $id, ApiContext $apiContext, BookingObjectHandler $bookingObjectHandler)
    {
        $error = null;
        $object = null;
        try {
            $data = $apiContext->showObjectReservation($id);
            $object = $bookingObjectHandler->showOneObject($data);
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        return $this->render('objects/object_booking.html.twig', array(
            'object' => $object
        ));
    }

}



