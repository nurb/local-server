<?php
namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ULoginRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('passport')
            ->add('roles', ChoiceType::class, [
                'placeholder' => 'Select role',
                'choices' => [
                    'Tenant' => 'tenant',
                    'Landlord' => 'landlord'
                ]
            ])
            ->add('save', SubmitType::class)
        ;
        /** @var Client $client */
        $client = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$client) {
                    $currentRoles = $client->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($tagsAsString) use (&$client) {
                    $currentRoles = array_diff($client->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($tagsAsString) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $client->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));
    }
}
