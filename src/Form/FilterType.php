<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'By name',
                'required' => false,
                'attr' => array(
                    'placeholder' => '',
                ),
            ))
            ->add('min_rate', TextType::class, array(
                'label' => 'Minimal price',
                'data' => '1',
                'attr' => array(
                    'placeholder' => '',
                ),
            ))
            ->add('max_rate', TextType::class, array(
                'label' => 'Maximum price',
                'data' => '9999',
                'attr' => array(
                    'placeholder' => '',
                ),
            ))
            ->add('type', ChoiceType::class, [
                'label' => 'Property type',
                'data' => '',
                'required' => false,
                'choices' => [
                    'Any' => '',
                    'Cottage' => 'cottage',
                    'Pension' => 'pension'
                ]
            ])

            ->add('login', SubmitType::class,
                array('label' => 'Search',
                    'attr' => ["class" => "btn btn-primary"]
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_client';
    }


}
