<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Name',
                'attr' => array(
                    'placeholder' => 'Property name',
                ),
            ))
            ->add('numberOfRooms', NumberType::class, array(
                'label' => 'Number of rooms',
                'attr' => array(
                    'placeholder' => 'How many numbers available to sell',
                ),
            ))
            ->add('contact', TextType::class, array(
                'label' => 'Contacts',

                'attr' => array(
                    'placeholder' => '0555123123',
                    'pattern' => '[0][0-9]{9}',
                ),
            ))
            ->add('address', TextType::class, array(
                'label' => 'Address',
                'data' => '42.64202041984265, 77.08450683593753',
                'attr' => array(
                    'placeholder' => 'Select on a map',
                    'readonly' => true,
                ),
            ))
            ->add('rates', NumberType::class, array(
                'label' => 'Rates',
                'attr' => array(
                    'placeholder' => 'rates per night',
                ),
            ))

            ->add('type', ChoiceType::class, [
                'label' => 'Property type',
                'placeholder' => 'Select an option',
                'choices' => [
                    'Cottage' => 'cottage',
                    'Pension' => 'pension'
                ]
            ])
            ->add('next', SubmitType::class,
                array('label' => 'Далее',
                    'attr' => ["class" => "btn btn-success"]
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\BookingObject'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_client';
    }


}
