<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PensionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('breakfast', CheckboxType::class, array(
                'label' => 'with Breakfast',
                'required' => false,
                'attr' => array(
                    'placeholder' => '',
                ),
            ))
            ->add('laundry', CheckboxType::class, array(
                'label' => 'Laundry Service',
                'required' => false,
                'attr' => array(
                    'placeholder' => '',
                ),
            ))
            ->add('next', SubmitType::class,
                array('label' => 'Next',
                    'attr' => ["class" => "btn btn-success"]
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Pension'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_client';
    }


}
