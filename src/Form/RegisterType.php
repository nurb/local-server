<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'example@mail.com',
                ),
            ))
            ->add('passport', TextType::class, array(
                'label' => 'Паспорт',
                'attr' => array(
                    'placeholder' => 'ID number',
                ),
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Password must match',
                'first_options' => array(
                    'label' => 'Password',
                    'attr' => array(
                        'placeholder' => 'Password',
                    ),
                ),
                'second_options' => array(

                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Repeat password',
                    ),
                ),
            ))

            ->add('roles', ChoiceType::class, [
                'placeholder' => 'Select role',
                'choices' => [
                    'Tenant' => 'tenant',
                    'Landlord' => 'landlord'
                ]
            ])
            ->add('save', SubmitType::class,
                array('label' => 'Done',
                    'attr' => ["class" => "btn btn-success"]
                ));

        /** @var Client $client */
        $client = $builder->getData();

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($storedRoles) use (&$client) {
                    $currentRoles = $client->getRoles();
                    if (in_array('ROLE_TENANT', $currentRoles)) {
                        return 'tenant';
                    } elseif (in_array('ROLE_LANDLORD', $currentRoles)) {
                        return 'landlord';
                    } else {
                        return null;
                    }
                },
                function ($tagsAsString) use (&$client) {
                    $currentRoles = array_diff($client->getRoles(), [
                        'ROLE_TENANT',
                        'ROLE_LANDLORD',
                    ]);

                    switch ($tagsAsString) {
                        case 'tenant':
                            $currentRoles[] = 'ROLE_TENANT';
                            break;
                        case 'landlord':
                            $currentRoles[] = 'ROLE_LANDLORD';
                            break;
                        default:
                            $currentRoles = $client->getRoles();
                            break;
                    }

                    return $currentRoles;
                }
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Client'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_client';
    }


}
