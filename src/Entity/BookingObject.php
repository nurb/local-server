<?php
/**
 * Created by PhpStorm.
 * User: nurb
 * Date: 02.07.2018
 * Time: 00:21
 */

namespace App\Entity;


class BookingObject
{
    protected $id;

    /** @var  $name string */
    protected $name;

    /** @var  $contact string */
    protected $contact;

    /** @var  $$numberOfRooms integer */
    protected $numberOfRooms;

    /** @var  $address string */
    protected $address;

    /** @var  $type string */
    protected $type;

    /** @var  $$rates float */
    protected $rates;

    /** @var  $$rates float */
    protected $bookings;

    /**
     * @param string $name
     * @return BookingObject
     */
    public function setName(string $name): BookingObject
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $contact
     * @return BookingObject
     */
    public function setContact(string $contact): BookingObject
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @param string $address
     * @return BookingObject
     */
    public function setAddress(string $address): BookingObject
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param float $rates
     * @return BookingObject
     */
    public function setRates(float $rates): BookingObject
    {
        $this->rates = $rates;
        return $this;
    }

    /**
     * @return float
     */
    public function getRates(): ?float
    {
        return $this->rates;
    }

    /**
     * @param mixed $numberOfRooms
     * @return BookingObject
     */
    public function setNumberOfRooms($numberOfRooms)
    {
        $this->numberOfRooms = $numberOfRooms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfRooms()
    {
        return $this->numberOfRooms;
    }

    /**
     * @param string $type
     * @return BookingObject
     */
    public function setType(string $type): BookingObject
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param mixed $id
     * @return BookingObject
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $bookings
     * @return BookingObject
     */
    public function setBookings($bookings)
    {
        $this->bookings = $bookings;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookings()
    {
        return $this->bookings;
    }


}