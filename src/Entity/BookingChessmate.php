<?php

namespace App\Entity;

use DateTime;


class BookingChessmate
{
    /**
     * @var int
     */
    private $id;


    private $checkIn;


    private $checkOut;


    private $booking_object;


    private $tenant;


    public function getId(): int
    {
        return $this->id;
    }


    public function setCheckIn($checkIn): BookingChessmate
    {
        $this->checkIn = $checkIn;
        return $this;
    }

    /**
     * @param $checkOut
     * @return BookingChessmate
     */
    public function setCheckOut($checkOut): BookingChessmate
    {
        $this->checkOut = $checkOut;
        return $this;
    }

    /**
     * @param $booking_object
     * @return BookingChessmate
     */
    public function setBookingObject($booking_object): BookingChessmate
    {
        $this->booking_object = $booking_object;
        return $this;
    }

    /**
     * @param $tenant
     * @return BookingChessmate
     */
    public function setTenant($tenant): BookingChessmate
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @return string
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @return BookingObject
     */
    public function getBookingObject(): BookingObject
    {
        return $this->booking_object;
    }

    /**
     * @return Client
     */
    public function getTenant(): Client
    {
        return $this->tenant;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'check-in' => $this->getCheckIn(),
            'check-out' => $this->getCheckOut(),
            'tenant' => $this->getTenant(),
            'booking_object' => $this->getBookingObject()
        ];
    }

    /**
     * @param int $id
     * @return BookingChessmate
     */
    public function setId(int $id): BookingChessmate
    {
        $this->id = $id;
        return $this;
    }


}