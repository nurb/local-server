<?php


namespace App\Entity;


use Symfony\Component\Security\Core\User\UserInterface;

class Pension extends BookingObject
{
    /** @var $laundry bool */
    private $laundry;

    /** @var $breakfast bool */
    private $breakfast;

    /**
     * @param bool $laundry
     * @return Pension
     */
    public function setLaundry(bool $laundry): Pension
    {
        $this->laundry = $laundry;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLaundry(): ?bool
    {
        return $this->laundry;
    }

    /**
     * @param bool $breakfast
     * @return Pension
     */
    public function setBreakfast(bool $breakfast): Pension
    {
        $this->breakfast = $breakfast;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBreakfast(): ?bool
    {
        return $this->breakfast;
    }

    public function toArray(Client $client)
    {
        return [
            'name' => $this->getName(),
            'contact' => $this->getContact(),
            'address' => $this->getAddress(),
            'rates' => $this->getRates(),
            'number_of_rooms' => $this->getNumberOfRooms(),
            'type' => $this->getType(),
            'breakfast' => $this->isBreakfast(),
            'laundry' => $this->isLaundry(),
            'owner' => $client->getUsername()
        ];
    }
}