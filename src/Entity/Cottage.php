<?php
/**
 * Created by PhpStorm.
 * User: nurb
 * Date: 02.07.2018
 * Time: 00:22
 */

namespace App\Entity;


use Symfony\Component\Security\Core\User\UserInterface;

class Cottage extends BookingObject
{
    /** @var $kitchen bool */
    protected $kitchen;

    /** @var $terrace bool */
    protected $terrace;

    /**
     * @param bool $terrace
     * @return Cottage
     */
    public function setTerrace(bool $terrace): Cottage
    {
        $this->terrace = $terrace;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTerrace(): ?bool
    {
        return $this->terrace;
    }

    /**
     * @param bool $kitchen
     * @return Cottage
     */
    public function setKitchen(bool $kitchen): Cottage
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return bool
     */
    public function isKitchen(): ?bool
    {
        return $this->kitchen;
    }

    public function toArray(Client $client)
    {
        return [
            'name' => $this->getName(),
            'contact' => $this->getContact(),
            'address' => $this->getAddress(),
            'rates' => $this->getRates(),
            'number_of_rooms' => $this->getNumberOfRooms(),
            'type' => $this->getType(),
            'kitchen' => $this->isKitchen(),
            'terrace' => $this->isTerrace(),
            'owner' => $client->getUsername()
        ];
    }

}