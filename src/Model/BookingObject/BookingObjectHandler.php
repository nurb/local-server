<?php


namespace App\Model\BookingObject;

use App\Entity\BookingChessmate;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Exception;

class BookingObjectHandler
{
    public function showObjects(ApiContext $apiContext, array $data)
    {

        $allRawObjects = $apiContext->getObjects([
            'name' => $data['name'],
            'type' => $data['type'],
            'min_rate' => $data['min_rate'],
            'max_rate' => $data['max_rate'],
        ]);
        $objects = $this->ClassificateObjects($allRawObjects);

        return $objects;
    }

    public function showOneObject($data)
    {
        $objects = $this->ClassificateObjects([$data]);
        return $objects[0];
    }


    /**
     * @param $allRawObjects
     * @return array
     * @throws Exception
     * @internal param $objects
     */
    private function ClassificateObjects($allRawObjects): array
    {
        $objects = [];
        foreach ($allRawObjects as $rawObject) {
            switch ($rawObject['type']) {
                case BookingObjectType::COTTAGE:
                    $classificatedObject = new Cottage();
                    $classificatedObject->setTerrace($rawObject['terrace']);
                    $classificatedObject->setKitchen($rawObject['kitchen']);
                    break;
                case BookingObjectType::PENSION:
                    $classificatedObject = new Pension();
                    $classificatedObject->setBreakfast($rawObject['breakfast']);
                    $classificatedObject->setLaundry($rawObject['laundry']);
                    break;
                default:
                    throw new Exception();
                    break;
            }
            $rooms = $rawObject['number_of_rooms'];
            $bookDate = $rawObject['bookings'];

            $classificatedObject->setId($rawObject['id']);
            $classificatedObject->setName($rawObject['name']);
            $classificatedObject->setContact($rawObject['contact']);
            $classificatedObject->setAddress($rawObject['address']);
            $classificatedObject->setNumberOfRooms($rooms);
            $classificatedObject->setRates($rawObject['rates']);
            $classificatedObject->setType($rawObject['type']);
            $bookings = [];

            foreach ($bookDate as $key => $rawBooking) {
                if ($key >= count($bookDate) - $rooms - 1) {
                    $booking = new BookingChessmate();
                    $booking->setId($rawBooking['id']);
                    $booking->setCheckIn($rawBooking['check-in']);
                    $booking->setCheckOut($rawBooking['check-out']);
                    $booking->setTenant($rawBooking['tenant']);
                    $booking->setBookingObject($rawBooking['booking_object']);
                    $bookings[] = $booking;
                }

            }
            $classificatedObject->setBookings($bookings);
            $objects[] = $classificatedObject;

        }
        return $objects;
    }

}