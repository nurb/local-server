<?php
/**
 * Created by PhpStorm.
 * User: nurb
 * Date: 02.07.2018
 * Time: 19:37
 */

namespace App\Model\BookingObject;


class BookingObjectType
{
    const COTTAGE = 'cottage';
    const PENSION = 'pension';
}