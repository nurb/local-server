<?php

namespace App\Model\Client;

use App\Entity\Client;
use App\Model\Api\ApiContext;
use App\Repository\ClientRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ClientHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(
        ContainerInterface $container,
        ApiContext $apiContext
    )
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }


    /**
     * @param array $data
     * @param bool $encodePassword
     * @return Client
     * @throws \App\Model\Api\ApiException
     */
    public function createNewClient(array $data, bool $encodePassword = true)
    {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        $client->setRoles($data['roles'] ?? ['ROLE_USER']);

        if ($encodePassword) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $client->setPassword($password);

        return $client;
    }


    /**
     * @param $rawUserData
     * @param $client Client
     * @return array
     */
    public function bindSocialAccount(array $rawUserData, Client $client): array
    {
        switch ($rawUserData['network']) {
            case self::SOC_NETWORK_VKONTAKTE:
                $client->setVkId($rawUserData['uid']);
                break;
            case self::SOC_NETWORK_GOOGLE:
                $client->setGoogleId($rawUserData['uid']);
                break;
            case self::SOC_NETWORK_FACEBOOK:
                $client->setFacebookId($rawUserData['uid']);
                break;
            default:
                break;
        }

        $data = [
            'email' => $client->getEmail(),
            'passport' => $client->getPassport(),
            'password' => $client->getPassword(),
            'vkId' => $client->getVkId(),
            'faceBookId' => $client->getFaceBookId(),
            'googleId' => $client->getGoogleId(),
        ];
        return $data;
    }


    /**
     * @param array $rawUserData
     * @param ClientRepository $clientRepository
     * @return Client|null $client Client
     */
    public function uLoginAuth(array $rawUserData, ClientRepository $clientRepository): ?Client
    {
        switch ($rawUserData['network']) {
            case self::SOC_NETWORK_VKONTAKTE:
                $client = $clientRepository->findOneByVk($rawUserData['uid']);
                break;
            case self::SOC_NETWORK_GOOGLE:
                $client = $clientRepository->findOneByGoogle($rawUserData['uid']);
                break;
            case self::SOC_NETWORK_FACEBOOK:
                $client = $clientRepository->findOneByFacebook($rawUserData['uid']);
                break;
            default:
                $client = null;
                break;
        }

        return $client;
    }


    /**
     * @param array $data
     * @param Client $client
     * @return Client
     * @internal param ObjectManager $manager
     */
    public function updateClient(array $data, Client $client)
    {
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setPassword($data['password']);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        return $client;
    }


    /**
     * @param Client $client
     */
    public function makeClientSession(Client $client)
    {
        $token = new UsernamePasswordToken($client, null, 'main', $client->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }

    /**
     * @param string $password
     * @return string
     * @throws \App\Model\Api\ApiException
     */
    public function encodePlainPassword(string $password): string
    {
        return $this->apiContext->encodePassword($password);
    }
}
