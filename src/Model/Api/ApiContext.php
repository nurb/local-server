<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client/reg';
    const ENDPOINT_CLIENT_UPDATE = '/client/update';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/client/check_client_credentials/{email}/{password}';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_SOCIAL_ID = '/client/social_id/{type}/{id}';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';

    const ENDPOINT_ADD_BOOKING_OBJECT = '/booking-object/add/{type}';
    const ENDPOINT_SHOW_OBJECTS = '/booking-object/show';
    const ENDPOINT_SHOW_ONE_OBJECT = '/booking-object/show/{id}';
    const ENDPOINT_BOOK_OBJECT = '/booking-object/book';
    const ENDPOINT_BOOK_ACTUAL = '/booking-object/actual/{email}';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function isActualBookingExists(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_BOOK_ACTUAL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     * @param string $password
     * @return array
     * @throws ApiException
     */
    public function checkClientCredentials(string $email, string $password)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'email' => $email,
            'password' => $this->encodePassword($password)
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }


    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }


    /**
     * @param string $type
     * @param string $id
     * @return array
     */
    public function getClientBySocialId(string $type, string $id)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_SOCIAL_ID, [
            'type' => $type,
            'id' => $id
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function getObjects(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_SHOW_OBJECTS, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @param string $type
     * @return mixed
     */
    public function createBookingObject(array $data, string $type)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_ADD_BOOKING_OBJECT, [
            'type' => $type
        ]);
        return $this->makeQuery($endPoint, self::METHOD_POST, $data);
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function showObjectReservation(int $id)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_SHOW_ONE_OBJECT, [
            'id' => $id
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function bookObject(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_BOOK_OBJECT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function updateClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_UPDATE, self::METHOD_POST, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }
}
