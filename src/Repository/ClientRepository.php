<?php

namespace App\Repository;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(
        RegistryInterface $registry,
        ClientHandler $clientHandler
    )
    {
        parent::__construct($registry, Client::class);
        $this->clientHandler = $clientHandler;
    }

    /**
     * @param string $email
     * @param string $plainPassword
     * @return Client|null
     */
    public function getByCredentials(string $email, string $plainPassword)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->andWhere('a.password = :password')
                ->setParameter(
                    'password',
                    $this->clientHandler->encodePlainPassword($plainPassword)
                )
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findOneByEmail($email): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email= :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByVk($vk): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.vkId = :vkId')
                ->setParameter('vkId', $vk)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByGoogle($google): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->Where('a.googleId= :gooleId')
                ->setParameter('gooleId', $google)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByFacebook($facebook): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->Where('a.faceBookId= :facebookId')
                ->setParameter('facebookId', $facebook)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findOneByPassport($passport): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport= :passport')
                ->setParameter('passport', $passport)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}