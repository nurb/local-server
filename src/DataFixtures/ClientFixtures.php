<?php


namespace App\DataFixtures;

use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $tenant = $this->clientHandler->createNewClient([
            'email' => 'rick@gmail.com',
            'passport' => 'AN123456',
            'password' => 'qwerty',
            'roles' => ["ROLE_USER","ROLE_TENANT"]
        ]);

        $manager->persist($tenant);

        $landlord = $this->clientHandler->createNewClient([
            'email' => 'morty@gmail.com',
            'passport' => 'AN654321',
            'password' => 'qwerty',
            'roles' => ["ROLE_USER","ROLE_LANDLORD"]
        ]);
        $manager->persist($landlord);
        $manager->flush();
    }
}
